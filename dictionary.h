#ifndef DICTIONARY_H
#define DICTIONARY_H

//Creates a handle for our dictionary object
typedef struct dictionary* Dictionary;

/*Creates a new Dictionary with all the slots initialised to NULL
 *@param initial_size the number of card "suits" to be sorted
 *@return the dictionary
 */
Dictionary createDictionary(int initial_size);

/* Creates a hash value for key in the dictionary
 * @param *key the string we want to sort
 * @param size_of_array the number of slots in our array
 * @return the index we wish to insert into
 */
int hash(char *key, int size_of_array);

/*Inserts a key into our dictionary, building it
 * @param dict the object dictionary
 * @param *key the string of the suit
 * @param value the position in the sorted array
 */
void insert(Dictionary dict, char *key, int value);

/* Finds the index at which a key should be. Speeds up searching.
 * @param dict The dictionary object searched
 * @param *key the string which is searched for
 * @result The index at which the suit is in.
 */
int get(Dictionary dict, char *key);

//Frees the dict object
void freeDictionary(Dictionary dict);
#endif

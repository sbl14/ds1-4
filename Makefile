CC = gcc
FLAGS = -Wall -g
SOURCES = client.c 
OBJECTS = dictionary.c scanner.c

cardSort.out : client.c
	$(CC) $(FLAGS) $(SOURCES) $(OBJECTS) -o $@

clean :
	rm -rf *.out

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scanner.h"
#include "dictionary.h"


struct card{
  int number;
  char* suit;
};

typedef struct card* Card;

Card createCard(char *suit, int rank);
void cardSwap(Card *cards, int a, int b);
void sortCards(Card *cards, Dictionary suitPositions, int numOfCards);
void printCards(Card *cards, int numOfCards);

int main(int argc, char **argv){
  int n, m, value;
  char* string;
  FILE *fp = fopen(argv[1], "r");

  n = readInt(fp);
  m = readInt(fp);

  Dictionary firstDictionary = createDictionary(n);

  Card *cards = malloc(m*sizeof(Card));//cards is an array of m Card pointers

  int i;
  for (i = 1; i <= n; ++i){
    string = readString(fp);
    value = i;
    insert(firstDictionary,string,value);
  }
  
  for (i = 0; i < m; ++i){
    string = readString(fp);
    value = readInt(fp);
    cards[i] = createCard(string, value);
  }

 
  sortCards(cards,firstDictionary,m);
  printCards(cards, m);
  
  free(string);
  for (i = 0; i < m; ++i){
    free(cards[i]);
  }
  free(cards);
  freeDictionary(firstDictionary);
  return 1;
}

Card createCard(char *suit, int rank){
  Card newCard = malloc(sizeof *newCard);
  newCard->number = rank;
  newCard->suit = suit;
  
  return newCard;
}

void cardSwap(Card *cards, int a, int b){
  Card tempCard = cards[a];//createCard(cards[a]->suit,cards[a]->number);
  cards[a] = cards[b];
  cards[b] = tempCard;
}

// This function sorts the cards array, first by rank and then by suit.
void sortCards(Card *cards, Dictionary suitPositions, int numOfCards){
  int i, j;
  
  for (i = 0; i < numOfCards; ++i){
    j = i;
    while(j > 0 && cards[j]->number < cards[j-1]->number){
      cardSwap(cards,j,j-1);
      --j;
    }
  }
  
  for (i = 0; i < numOfCards; ++i){
    j = i;
    while(j > 0 && get(suitPositions,cards[j]->suit) < get(suitPositions,cards[j-1]->suit)){
      cardSwap(cards,j,j-1);
      --j;
    }
  }
}

void printCards(Card *cards, int numOfCards){
  int i;
    for (i = 0; i < numOfCards; ++i){
      printf("%s %d\n",cards[i]->suit,cards[i]->number);
    }
}

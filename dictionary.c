#include <stdlib.h>
#include <string.h>

#include "dictionary.h"
/* This struct holds a suit and its relative position in the ordering
 */
struct suit{
  int value;
  char* key;
};

typedef struct suit *Suit;

struct dictionary{
  int size;
  Suit* slot;//This will be an array of Suits that we malloc to size intial_size
};

// Creates a dictionary, which is the order which we need to sort the cards
Dictionary createDictionary(int initial_size){
  Dictionary newDict = malloc(sizeof *newDict);
  newDict->slot = malloc(initial_size*sizeof(Suit));

  newDict->size = initial_size;
  
  int i;
  for (i = 0; i < newDict->size; ++i){
    newDict->slot[i] = NULL;
  }
  return newDict;
}
// Helper function to create a suit, which will be called by insert
static Suit createSuit(int a, char *b){
  Suit newSuit = malloc(sizeof *newSuit);
  newSuit->value = a;
  newSuit->key = b;

  return newSuit;
}

//Returns the index value of a key in the array of suits
int hash(char *key, int size_of_array){
  int position = 0;
  while(*key != '\0'){
    position += *key++;
  }
  position = position % size_of_array;

  return position;
}

//Inserts a single key value pair into the dictionary
void insert(Dictionary dict, char *key, int value){
  Suit newSuit = createSuit(value, key);
  int index = hash(key,dict->size);

  while (dict->slot[index % dict->size] != NULL){
   ++index;
  }
  
  dict->slot[index % dict->size] = newSuit;
}

//Retrieves the position that a @key card should be sorted
int get(Dictionary dict, char *a){
  int index = hash(a,dict->size);

  while (strcmp(a,dict->slot[index%dict->size]->key) != 0){
      ++index;
    }
  return dict->slot[index%dict->size]->value;
}

//Frees the dictionary structure fully
void freeDictionary(Dictionary dict){
  int i;
  for (i = 0; i < dict->size; ++i){
    free(dict->slot[i]);
  }
  
  free(dict);
}
